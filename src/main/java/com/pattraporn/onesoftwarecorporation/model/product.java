/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.onesoftwarecorporation.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roman
 */
public class product {

    private int id;
    private String name;
    private double price;
    private int sweetlevel;
    private String size;
    private String type;
    private String category;
    

    public product() {
        this.id = -1;
    }
    public product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }


    public product(int id, String name, double price, int sweetlevel, String size, String type, String category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.sweetlevel = sweetlevel;
        this.size = size;
        this.type = type;
        this.category = category;
    }

    public product(String name, double price, int sweetlevel, String size, String type, String category) {
        this.id = -1;
        this.name = name;
        this.price = price;
        this.sweetlevel = sweetlevel;
        this.size = size;
        this.type = type;
        this.category = category;
    }

    public int getSweetlevel() {
        return sweetlevel;
    }

    public void setSweetlevel(int sweetlevel) {
        this.sweetlevel = sweetlevel;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    

    @Override
    public String toString() {
        return "product{" + "id=" + id + ", name=" + name + ", price=" + price + ",sweetlevel=" + sweetlevel + ",size=" + size + ",type=" + type + ",category=" + category + '}';
    }

    public static product fromRS(ResultSet rs) {
        product product = new product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setName(rs.getString("product_name"));
            product.setPrice(rs.getDouble("product_price"));
            product.setSweetlevel(rs.getInt("product_sweetlevel"));
            product.setSize(rs.getString("product_size"));
            product.setType(rs.getString("product_type"));
            product.setCategory(rs.getString("product_category"));

        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;

        }
        return product;
    }
    public static ArrayList<product> getMockProductList(){
        ArrayList<product> list = new ArrayList<product>();
        for(int i=0;i<10;i++){
        list.add(new product(i+1, "Coffee"+ i,(i+1)*5));
    }
        return list;
    }
}
