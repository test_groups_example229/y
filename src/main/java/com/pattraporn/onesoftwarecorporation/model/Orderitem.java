/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.model;

/**
 *
 * @author satit
 */
public class Orderitem {
    product product;
    int amount;

    public Orderitem(product product, int amount) {
        this.product = product;
        this.amount = amount;
    }

    public product getProduct() {
        return product;
    }

    public void setProduct(product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
}
