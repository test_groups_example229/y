/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.service;


import com.pattraporn.onesoftwarecorporation.dao.SaleDao;
import com.pattraporn.onesoftwarecorporation.model.ReportSale;
import java.util.List;

/**
 *
 * @author User
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay(){
        SaleDao dao = new SaleDao();
       return dao.getDayReport();
        
    }
     public List<ReportSale> getReportSaleByMonth(int year){
        SaleDao dao = new SaleDao();
       return dao.getMonthReport(year);
        
    }
}
