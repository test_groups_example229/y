/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.pattraporn.onesoftwarecorporation.ui;

import com.pattraporn.onesoftwarecorporation.model.product;

/**
 *
 * @author satit
 */
public interface OnBuyProductLister {
    public void buy(product product, int amount);
    
}
