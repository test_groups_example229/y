--
-- File generated with SQLiteStudio v3.3.3 on Sun Oct 9 00:59:58 2022
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Bill
DROP TABLE IF EXISTS Bill;

CREATE TABLE Bill (
    Bil_ID     INTEGER PRIMARY KEY,
    Bill_price INTEGER,
    Bill_total INTEGER,
    Bill_Date  DATE
);


-- Table: Category
DROP TABLE IF EXISTS Category;

CREATE TABLE Category (
    category_id   INTEGER PRIMARY KEY,
    category_name TEXT
);


-- Table: CheckIn
DROP TABLE IF EXISTS CheckIn;

CREATE TABLE CheckIn (
    CI_ID   INTEGER PRIMARY KEY,
    CI_TIME TIME,
    CI_DATE DATE,
    EM_ID   INTEGER REFERENCES Employee (EM_ID) 
);


-- Table: CheckOut
DROP TABLE IF EXISTS CheckOut;

CREATE TABLE CheckOut (
    CO_ID   INTEGER PRIMARY KEY,
    CO_TIME TIME,
    CO_DATE DATE,
    EM_ID   INTEGER REFERENCES Employee (EM_ID) 
);


-- Table: Customer
DROP TABLE IF EXISTS Customer;

CREATE TABLE Customer (
    Cus_ID          INTEGER    PRIMARY KEY,
    Cus_name        TEXT,
    Cus_Phonenumber INTEGER,
    Cus_Address     TEXT (100) 
);


-- Table: Employee
DROP TABLE IF EXISTS Employee;

CREATE TABLE Employee (
    EM_ID         INTEGER   PRIMARY KEY,
    EM_Firstname  TEXT (50),
    EM_Surname    TEXT (50),
    EM_Position   TEXT (50),
    EM_IP_Address INTEGER,
    EM_Phone      INTEGER,
    EM_Salary     INTEGER
);


-- Table: Order
DROP TABLE IF EXISTS [Order];

CREATE TABLE [Order] (
    Order_ID       INTEGER   PRIMARY KEY,
    Order_Name     TEXT (50),
    Oder_Date      DATE,
    Order_Time     TIME,
    Order_total    INTEGER,
    Order_discount INTEGER,
    Order_cash     INTEGER,
    Order_change   INTEGER,
    Cus_ID         INTEGER   REFERENCES customer (Cus_ID),
    Store_ID       INTEGER   REFERENCES Store (Store_ID),
    EM_ID          INTEGER   REFERENCES Employee (EM_ID) 
);


-- Table: Order_item
DROP TABLE IF EXISTS Order_item;

CREATE TABLE Order_item (
    Order_item_ID     INTEGER   PRIMARY KEY,
    Order_item_Name   TEXT (50),
    Order_item_Amount INTEGER,
    Order_item_Price  INTEGER,
    Order_total       INTEGER   REFERENCES [Order] (Order_total),
    Order_ID          INTEGER   REFERENCES [Order] (Order_ID) 
);


-- Table: Product
DROP TABLE IF EXISTS Product;

CREATE TABLE Product (
    Product_ID         INTEGER   PRIMARY KEY,
    Product_Name       TEXT (50),
    Product_Price      INTEGER,
    Product_sweetLevel INTEGER,
    Product_image,
    Product_size       TEXT,
    Product_type       INTEGER,
    category_id        INTEGER   REFERENCES Category (category_id) 
);


-- Table: Stock
DROP TABLE IF EXISTS Stock;

CREATE TABLE Stock (
    Stock_ID      INTEGER   PRIMARY KEY,
    Stock_Name    TEXT (50),
    Stock_Minimum INTEGER,
    Stock_remain  INTEGER,
    Stock_type    CHAR,
    Stock_Date    DATE
);


-- Table: Stock Details
DROP TABLE IF EXISTS [Stock Details];

CREATE TABLE [Stock Details] (
    STD_ID       INTEGER PRIMARY KEY,
    Stock_name   TEXT    REFERENCES Stock (Stock_Name),
    STD_quantity INTEGER,
    Stock_remain INTEGER REFERENCES Stock (Stock_remain),
    STD_Date     DATE
);


-- Table: StockM
DROP TABLE IF EXISTS StockM;

CREATE TABLE StockM (
    Stock_name   TEXT    REFERENCES Stock (Stock_Name),
    Stock_remain INTEGER REFERENCES Stock (Stock_remain) 
);


-- Table: Store
DROP TABLE IF EXISTS Store;

CREATE TABLE Store (
    Store_ID      INTEGER    PRIMARY KEY,
    Store_name    TEXT (50),
    Stroe_address TEXT (100),
    Store_tel     INTEGER,
    Store_tax     INTEGER,
    Store_logo
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
